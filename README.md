# Importation du BBS

Notes pour l'importation de sujets choisit dans le nouveau forum à partir de la base de donnée mongoDB.

Références :

- Script d'importation original : https://raw.githubusercontent.com/discourse/discourse/main/script/import_scripts/nodebb/nodebb.rb
- Tutoriel officiel : https://meta.discourse.org/t/migrate-a-nodebb-forum-with-mongodb-to-discourse/126553/11
- Billet de blog : https://odyslam.com/blog/Migrating-from-Nodebb-to-Discourse/

Documentation Python :

- https://pypi.org/project/fluent-discourse/
- https://pypi.org/project/discourse/

API Docs:

- https://docs.discourse.org/
- https://docs.nodebb.org/api